package com.src.brigi.advanced.mainshapes;

import com.src.brigi.advanced.shapes.Circle;
import com.src.brigi.advanced.shapes.Rectangle;
import com.src.brigi.advanced.shapes.Square;

public class MainShapes {

	public static void main(String[] args) {

		Circle circle1 = new Circle();
		Circle circle2 = new Circle();
		
		circle1.setRadius(2);
		circle2.setRadius(2);
		circle2.setColor("blue");
		circle2.setFilled(true);
		System.out.println("Circle1 radius: " + circle2.getRadius() + ", area: " + circle1.getArea() + ", perimeter: "
				+ circle1.getPerimeter());
		System.out.println("Circle2 radius: " + circle2.getRadius()+ ", area: " + circle2.getArea() + ", perimeter: "
				+ circle2.getPerimeter() + ", color: " + circle2.getColor() + ", filled: " + circle2.isFilled());

		
		Rectangle rectangle1 = new Rectangle();
		Rectangle rectangle2 = new Rectangle();
		rectangle1.setWidth(3);
		rectangle1.setLength(5);
		rectangle2.setWidth(4);
		rectangle2.setLength(7);
		rectangle2.setColor("red");
		rectangle2.setFilled(false);
		System.out.println("Rectangle1 area: " + rectangle1.getArea() + ", perimeter: " + rectangle1.getPerimeter());
		System.out.println("Rectangle2 area: " + rectangle2.getArea() + ", perimeter: " + rectangle2.getPerimeter()
				+ ", color: " + rectangle2.getColor() + ", filled: " + rectangle2.isFilled());

	
		Square square1 = new Square();
		Square square2 = new Square();
		square1.setSide(2);
		square2.setSide(4);
		square2.setColor("yellow");
		square2.setFilled(true);
		System.out.println("Square1 side width: " + square1.getSide() + ", side length: " + square1.getSide());
		System.out.println("Square2 side width: " + square2.getSide() + ", side length: " + square2.getSide()
				+ ", color: " + square2.getColor() + ", filled: " + square2.isFilled());
	}
}

