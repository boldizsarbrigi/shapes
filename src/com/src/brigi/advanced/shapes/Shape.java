package com.src.brigi.advanced.shapes;

//abstract class Shape
public abstract class Shape {

	protected String color;
	protected boolean filled;

	// default constructor
	public Shape() {
	}

	//defined constructor with parameters
	public Shape(String color, boolean filled) {
		this.color = color;
		this.filled = filled;
	}

	// getter and setter
	public String getColor() {
		return color;
	}

	public void setColor(String color) {
		this.color = color;
	}

	public boolean isFilled() {
		return filled;
	}

	public void setFilled(boolean filled) {
		this.filled = filled;
	}

	// abstract methods getArea and getPerimeter
	// abstract classes do not have body, the extended classes Circle, Rectangle
	// need to implement these 2 methods
	public abstract double getArea();

	public abstract double getPerimeter();

	// toString method
	@Override
	public String toString() {
		return "Shape [color=" + color + ", filled=" + filled + "]";
	}

}
