package com.src.brigi.advanced.shapes;

public class Square extends Rectangle {
	protected double side;

	// default constructor
	public Square() {
	}

	// defined constructor with parameters
	public Square(double side) {
		this.side = side;
	}

	// defined constructor with parameters
	public Square(double side, String color, boolean filled) {
		super(color, filled);
		this.side = side;

	}

	// getter and setter
	public double getSide() {
		return side;
	}

	public void setSide(double side) {
		this.side = side;
	}

	public void setWidth(double side) {
		this.side = side;
	}

	public void setLength(double side) {
		this.side = side;
	}

	// toString method
	@Override
	public String toString() {
		return "Square [side=" + side + ", width=" + width + ", length=" + length + ", color=" + color + ", filled="
				+ filled + "]";
	}

}
