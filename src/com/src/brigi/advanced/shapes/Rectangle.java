package com.src.brigi.advanced.shapes;

public class Rectangle extends Shape {
	protected double width;
	protected double length;

	// default constructor
	public Rectangle() {
	}

	// defined constructor with parameters
	public Rectangle(double width, double length) {
		this.width = width;
		this.length = length;
	}

	// defined constructor with parameters
	public Rectangle(String color, boolean filled) {
		super(color, filled);
	}

	// defined constructor with parameters
	public Rectangle(double width, double length, String color, boolean filled) {
		super(color, filled);
		this.width = width;
		this.length = length;
	}

	// getter and setter
	public double getWidth() {
		return width;
	}

	public void setWidth(double width) {
		this.width = width;
	}

	public double getLength() {
		return length;
	}

	public void setLength(double length) {
		this.length = length;
	}

	// implementing the abstract classes getArea and getPerimeter from class Shape
	@Override
	public double getArea() {
		return width * length;
		// area=width*length
	}

	@Override
	public double getPerimeter() {
		return 2 * (width + length);
		// perimetru=2*(width+length)
	}

	// toString method
	@Override
	public String toString() {
		return "Rectangle [width=" + width + ", length=" + length + "]" + "Shape [ color=" + color + ", filled="
				+ filled + "]";
	}

}
