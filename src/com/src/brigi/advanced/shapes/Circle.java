package com.src.brigi.advanced.shapes;

public class Circle extends Shape {

	protected double radius;
	double pi = Math.PI;

	// default constructor
	public Circle() {
	}

	// defined constructor with parameters
	public Circle(double radius) {
		this.radius = radius;
	}

	// defined constructor with parameters
	public Circle(double radius, String color, boolean filled) {
		super(color, filled);
		this.radius = radius;
	}

	// getter and setter
	public double getRadius() {
		return radius;
	}

	public void setRadius(double radius) {
		this.radius = radius;
	}

	// implementing the abstract classes getArea and getPerimeter from class Shape
	@Override
	public double getArea() {
		return pi * Math.pow(radius, 2);
		// area=pi(r^2)
	}

	@Override
	public double getPerimeter() {
		return 2 * pi * radius;
		// perimeter=2pir
	}

	// toString method
	@Override
	public String toString() {
		return "Circle [radius=" + radius + "]" + "Shape [ color=" + super.color + ", filled=" + super.filled + "]";
	}
}
